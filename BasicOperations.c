
#include "Cbitwicer.h"

void SetBit(uint32_t *Register, uint8_t BitToSet){
    *Register |= (1 << BitToSet);
};

void ClearBit(uint32_t *Register, uint8_t BitToClear){
    *Register &= ~(1 << BitToClear);
};

void ToggleBit(uint32_t *Register, uint8_t BitToToggle){
    *Register ^= (1 << BitToToggle);
};

void CheckBit(uint32_t *Register, uint8_t BitToCheck, uint8_t *IsSet){
    *IsSet = (*Register & (1 << BitToCheck));
};

void DecimalToBinaryString(uint32_t DecimalNumber, char *BinaryString){
    uint32_t RemainPart = DecimalNumber;
    uint8_t BitToSet = 0, Idx = 0, Jdx, Kdx;
    while (RemainPart > 0){
        BitToSet = RemainPart % 2;
        RemainPart /= 2;
        BinaryString[Idx++] = (BitToSet == 1) ? '1' : '0';
    };
    BinaryString[Idx] = '\0';
    for (Jdx= 0, Kdx=Idx; Jdx < Idx / 2 ; Jdx++){
        char TempChar = BinaryString[Jdx];
        BinaryString[Jdx] = BinaryString[--Kdx];
        BinaryString[Kdx] = TempChar;
    };
    printf("[DecimalToBinaryString]:%s\n", BinaryString);
};
