
#include "Cbitwicer.h"

void SetBit_Test(){
    uint32_t Register = 0b00000000000000000000000000000000;
    char BinaryString[sizeof(uint32_t)*8+1];
    srand(time(NULL)); uint8_t BitToSet = rand() % 33;
    printf("[SetBit_Test]: Setting %d bit to zero register\n", BitToSet);
    SetBit(&Register, BitToSet);
    DecimalToBinaryString(Register,BinaryString);
};

void ClearBit_Test(){
    uint32_t Register = 0b11111111111111111111111111111111;
    char BinaryString[sizeof(uint32_t)*8+1];
    srand(time(NULL)); uint8_t BitToClear = rand() % 33;
    printf("[ClearBit_Test]: Clearing %d bit to fulfiled register\n", BitToClear);
    ClearBit(&Register, BitToClear);
    DecimalToBinaryString(Register,BinaryString);
};

void ToggleBit_Test(){
    uint32_t RegisterOne = 0b11111111111111111111111111111111;
    uint32_t RegisterTwo = 0b00000000000000000000000000000000;
    char BinaryString[sizeof(uint32_t)*8+1];
    srand(time(NULL)); uint8_t BitToToggle = rand() % 33;
    printf("[ToggleBit_Test]: Toggling %d bit to fulfiled register\n", BitToToggle);
    ToggleBit(&RegisterOne, BitToToggle);
    DecimalToBinaryString(RegisterOne,BinaryString);
    printf("[ToggleBit_Test]: Toggling %d bit to zero register\n", BitToToggle);
    ToggleBit(&RegisterTwo, BitToToggle);
    DecimalToBinaryString(RegisterTwo,BinaryString);
};

void CheckBit_Test(){
    uint32_t Register = 0b11001010110101011011100110011010;
    char BinaryString[sizeof(uint32_t)*8+1];
    srand(time(NULL)); uint8_t BitToCheck = rand() % 33;
    printf("[CheckBit_Test]: Check %d bit of random register\n", BitToCheck);
    DecimalToBinaryString(Register,BinaryString);
    uint8_t IsSet;
    CheckBit(&Register, BitToCheck, &IsSet);
    printf("[CheckBit_Test]: bit value IsSet=%d\n", (IsSet > 0) ? 1 : 0);
};
