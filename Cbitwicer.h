
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
// Hex | Binary
// 0 	| 0000
// 1 	| 0001
// 2 	| 0010
// 3 	| 0011
// 4 	| 0100
// 5 	| 0101
// 6 	| 0110
// 7 	| 0111
// 8 	| 1000
// 9 	| 1001
// A 	| 1010
// B 	| 1011
// C 	| 1100
// D 	| 1101
// E 	| 1110
// F 	| 1111

//---------------------------------------------------------------------
void SetBit(uint32_t *Register, uint8_t BitToSet);
void ClearBit(uint32_t *Register, uint8_t BitToClear);
void ToggleBit(uint32_t *Register, uint8_t BitToToggle);
void CheckBit(uint32_t *Register, uint8_t BitToCheck, uint8_t *IsSet);
void DecimalToBinaryString(uint32_t DecimalNumber, char *BinaryString);
//---------------------------------------------------------------------
void SetBit_Test();
void ClearBit_Test();
void ToggleBit_Test();
void CheckBit_Test();
//---------------------------------------------------------------------
