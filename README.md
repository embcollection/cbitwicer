# cbitwicer
----------------------
## Bacis Operations
In C bit operators are “&”, “|”, “~”, “^”, “>>”, “<<“

### Set a Bit
![Alt text](Images/Image-1.webp?raw=true "Set a Bit")

### Clear a Bit
![Alt text](Images/Image-2.webp?raw=true "Clear a Bit")

### Toggle a Bit
![Alt text](Images/Image-3.webp?raw=true "Toggle a Bit")
